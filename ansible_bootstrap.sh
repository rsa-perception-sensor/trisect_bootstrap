#!/usr/bin/bash

set -e

if [[ -f $HOME/.trisect_bootstrap.sh ]]; then
    echo "Using configuration from ~/.trisect_bootstrap.sh"
    source ~/.trisect_bootstrap.sh
    echo ""
    echo "   TRISECT_HOSTNAME=$TRISECT_HOSTNAME"
    echo ""
else
    echo "Please set configuration in ~/.trisect_bootstrap.sh:"
    echo ""
    echo "   echo \"export TRISECT_HOSTNAME=<hostname>\" > ~/.trisect_bootstrap.sh"
    exit -1
fi

sudo apt install -y --no-install-recommends software-properties-common git
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install -y --no-install-recommends ansible

export BOOTSTRAP_BRANCH="dev/jetpack_5.1.4"

sudo ansible-pull -vvv \
            --extra-vars "var_hostname=$TRISECT_HOSTNAME" \
            --extra-vars "bootstrap_branch=$BOOTSTRAP_BRANCH" \
            -C $BOOTSTRAP_BRANCH \
            -U https://gitlab.com/rsa-perception-sensor/trisect_bootstrap \
            ansible/trisect.yml
