# trisect_bootstrap

Ansible scripts codifying the setup of a Trisect **from scratch**.

This branch is for **Jetpack 5.1.4**, which is based on Ubuntu 20.04 and requires less manual compilation of OpenCV and ROS.

For detail on how to bootstrap a Trisect from scratch, see the [trisect-docs](https://trisect-perception-sensor.gitlab.io/trisect-docs/docs/)
